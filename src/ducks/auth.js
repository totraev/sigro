import { handleActions, createAction } from 'redux-actions'
import { createFormAction } from 'redux-form-saga'
import { isAuth } from '../utils/auth'

export const login = createFormAction('auth/LOGIN')
export const logout = createAction('auth/LOGOUT')

const initState = {
  isAuthorized: isAuth()
}

const loginReducer = handleActions({
  [login.SUCCESS]: (state, action) => ({
    ...state,
    isAuthorized: true
  }),
  [login.FAILURE]: (state, action) => ({
    ...state,
    isAuthorized: false
  }),
  [logout]: (state, action) => ({
    ...state,
    isAuthorized: false
  })
}, initState)

export default loginReducer
