import { createSagaAction } from '../utils/saga'
import { handleActions } from 'redux-actions'

export const fetchFields = createSagaAction('fields/FETCH')

const initialState = {
  fields: []
}

const fieldsReducer = handleActions({
  [fetchFields.SUCCESS]: (state, { payload }) => ({
    ...state,
    fields: payload
  })
}, initialState)

export default fieldsReducer
