import { createAction, handleActions } from 'redux-actions'

export const setLang = createAction('app/SET_LANGUAGE')

const initState = {
  lang: 'ru'
}

const appReducer = handleActions({
  [setLang]: (state, {payload: lang}) => ({
    ...state,
    lang
  })
}, initState)

export default appReducer
