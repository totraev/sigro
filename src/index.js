import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { Router, browserHistory } from 'react-router'
import routes from './routes'
import store from './redux/store'
import { syncHistoryWithStore } from 'react-router-redux'

import './static/fonts/font-awesome/css/font-awesome.css'
import './static/fonts/simple-line-icons/simple-line-icons.css'
import './static/styles/bootstrap/css/bootstrap.css'
import './static/styles/components.css'
import './static/styles/layout.css'
import './static/styles/login-3.css'
import './static/styles/themes/light.css'
import './static/styles/styles.css'
import '../node_modules/leaflet/dist/leaflet.css'

const history = syncHistoryWithStore(browserHistory, store)

render(
  <Provider store={store}>
    <Router history={history} routes={routes} />
  </Provider>,
  document.getElementById('cloud')
)
