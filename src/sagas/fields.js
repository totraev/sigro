import { takeLatest } from 'redux-saga'
import { get } from '../utils/api'
import { fetchFields } from '../ducks/fields'
import { fetchSagaCreator } from '../utils/saga'

export function * fieldsSaga () {
  yield * takeLatest(
    fetchFields.REQUEST,
    fetchSagaCreator(fetchFields, get, '/fields/')
  )
}
