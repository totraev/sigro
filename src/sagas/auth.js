import { takeLatest } from 'redux-saga'
import { put, call, take } from 'redux-saga/effects'
import { login as loginAction } from '../ducks/auth'
import { login } from '../utils/api'
import { push } from 'react-router-redux'
import { setToken, removeToken } from '../utils/auth'

function * fetchLogin ({payload: {username, password}}) {
  try {
    const token = yield call(login, {username, password})
    const decoded = setToken(token)

    yield put(loginAction.success(decoded))
    yield put(push('/'))
  } catch (e) {
    yield put(loginAction.failure(e))
  }
}

export function * loginSaga () {
  yield * takeLatest(
    loginAction.REQUEST,
    fetchLogin
  )
}

export function * logoutSaga () {
  while (true) {
    yield take('auth/LOGOUT')
    yield call(removeToken)
  }
}
