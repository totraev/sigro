import { formActionSaga } from 'redux-form-saga'

import { loginSaga, logoutSaga } from './auth'
import { fieldsSaga } from './fields'

export default function * () {
  yield [
    formActionSaga(),
    loginSaga(),
    logoutSaga(),
    fieldsSaga()
  ]
}
