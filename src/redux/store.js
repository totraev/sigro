import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import createLoggerMiddleware from 'redux-logger'
import { browserHistory } from 'react-router'
import { routerMiddleware } from 'react-router-redux'
import rootReducer from './reducer'
import rootSaga from '../sagas'

const sagaMiddleware = createSagaMiddleware()

const middlewares = [
  routerMiddleware(browserHistory),
  createLoggerMiddleware(),
  sagaMiddleware
]

const store = createStore(
  rootReducer,
  applyMiddleware(...middlewares)
)

sagaMiddleware.run(rootSaga)

export default store
