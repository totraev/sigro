import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { reducer as formReducer } from 'redux-form'

import * as ducksReducers from '../ducks'

const rootReducer = combineReducers({
  routing: routerReducer,
  form: formReducer,
  ...ducksReducers
})

export default rootReducer
