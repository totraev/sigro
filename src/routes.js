import React from 'react'
import { Route, IndexRedirect } from 'react-router'
import { UserAuthWrapper } from 'redux-auth-wrapper'
import { push } from 'react-router-redux'

import App from './components/environment/App'

import AppLayout from './components/ecosystems/AppLayout'
import AuthLayout from './components/ecosystems/AuthLayout'
import SingIn from './components/ecosystems/SingIn'
import Fields from './components/ecosystems/Fields'
import Field from './components/ecosystems/Field'
import FieldFeatures from './components/ecosystems/FieldFeatures'
import FieldOperations from './components/ecosystems/FieldOperations'
import FieldOperation from './components/ecosystems/FieldOperation'
import FieldAnalytics from './components/ecosystems/FieldAnalytics'
import FieldArea from './components/ecosystems/FieldArea'

const UserIsAuthenticated = UserAuthWrapper({
  authSelector: (state) => state.auth,
  predicate: (auth) => auth.isAuthorized,
  redirectAction: push,
  wrapperDisplayName: 'UserIsAuthenticated',
  failureRedirectPath: '/singin'
})

export default (
  <Route path='/' component={App}>
    <Route component={AuthLayout}>
      <Route path='singin' component={SingIn} />
    </Route>

    <Route component={UserIsAuthenticated(AppLayout)}>
      <IndexRedirect to='/fields' />
      <Route path='fields' component={Fields} />

      <Route path='fields/:id' component={Field}>
        <IndexRedirect to='features' />
        <Route path='features' component={FieldFeatures} />
        <Route path='operations' component={FieldOperations} />
        <Route path='operations/:opId' component={FieldOperation} />
        <Route path='analytics' component={FieldAnalytics} />
        <Route path='area' component={FieldArea} />
      </Route>
    </Route>
  </Route>
)
