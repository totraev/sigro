import React, { PropTypes } from 'react'

const propTypes = {
  to: PropTypes.string.isRequired
}

const contextTypes = {
  router: PropTypes.object
}

const RowLink = (props, context) => {
  const { to, children } = props
  const { router } = context

  const onClick = () => {
    router.push(to)
  }

  return <tr {...{ onClick }}>{children}</tr>
}

RowLink.propTypes = propTypes
RowLink.contextTypes = contextTypes

export default RowLink
