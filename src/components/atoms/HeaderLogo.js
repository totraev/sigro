import React from 'react'

const HeaderLogo = (props) => {
  return (
    <div className='page-logo'>
      <a href='/'>
        <img src={require('../../static/big-logo.jpg')} alt='logo' className='logo' />
      </a>
      <div className='menu-toggler sidebar-toggler' />
    </div>
  )
}

export default HeaderLogo
