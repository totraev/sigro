import React, { PropTypes } from 'react'

const propTypes = {
  name: PropTypes.string.isRequired
}

const Icon = (props) => {
  const { name } = props
  const iconClass = `icon-${name}`

  return <i className={iconClass} />
}

Icon.propTypes = propTypes

export default Icon
