import React, { PropTypes } from 'react'

const propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  checked: PropTypes.bool
}

const AuthCheckbox = (props) => {
  const { label, value, checked } = props

  return (
    <label className='rememberme mt-checkbox mt-checkbox-outline'>
      <input type='checkbox' name='remember' value={value} checked={checked}/> {label}
      <span />
    </label>
  )
}

AuthCheckbox.propTypes = propTypes

export default AuthCheckbox
