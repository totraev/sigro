import React, { PropTypes } from 'react'

const propTypes = {
  icon: PropTypes.element,
  title: PropTypes.string,
  helper: PropTypes.string,
  actions: PropTypes.element
}

const Portlet = (props) => {
  const {
    icon,
    title,
    helper,
    actions,
    children
  } = props

  const portletHeader = (
    <div className='portlet-title'>
      <div className='caption'>
        {icon}
        <span className='caption-subject bold uppercase'>{title}</span>
        {helper ? <span className='caption-helper'>{helper}</span> : null}
      </div>
      {actions}
    </div>
  )

  return (
    <div className='portlet light'>
      {title ? portletHeader : null}

      <div className='portlet-body'>
        {children}
      </div>
    </div>
  )
}

Portlet.propTypes = propTypes

export default Portlet
