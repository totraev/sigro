import React, { PropTypes } from 'react'
import { FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap'

const propTypes = {
  icon: PropTypes.element.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object,
  placeholder: PropTypes.string
}

const IconInput = (props) => {
  const {
    icon,
    input,
    label,
    type,
    placeholder,
    meta
  } = props

  const validationState = meta && meta.error ? { validationState: 'error' } : {}

  return (
    <FormGroup {...validationState}>
      <ControlLabel>{label}</ControlLabel>
      <div className='input-icon'>
        {icon}
        <FormControl type={type} placeholder={placeholder} {...input} />
      </div>
      {meta ? meta.touched && meta.error && <HelpBlock>{meta.error}</HelpBlock> : null}
    </FormGroup>
  )
}

IconInput.propTypes = propTypes

export default IconInput
