import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import classNames from 'classnames'

const propTypes = {
  icon: PropTypes.element,
  title: PropTypes.string.isRequired,
  active: PropTypes.bool,
  href: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ])
}

const SidebarItem = (props) => {
  const {
    icon,
    title,
    active,
    href
  } = props

  return (
    <li className={classNames('nav-item', { active })}>
      <Link className='nav-link nav-toggle' to={href}>
        {icon}
        <span className='title'>{title}</span>
      </Link>
    </li>
  )
}

SidebarItem.propTypes = propTypes

export default SidebarItem
