import React, { PropTypes } from 'react'
import { Breadcrumb } from 'react-bootstrap'
import FontAwesome from 'react-fontawesome'
import { LinkContainer } from 'react-router-bootstrap'

const propTypes = {
  location: PropTypes.object.isRequired
}

const AppBreadcrumbs = (props) => {
  const { location: { pathname } } = props
  const routes = pathname.substr(1).split('/')

  const joinPath = (idx) => {
    return '/' + routes.slice(0, idx + 1).join('/')
  }

  const capitalize = (string) => {
    return string[0].toUpperCase() + string.slice(1)
  }

  return (
    <Breadcrumb id='app-breadcrumbs'>
    {
      routes.map((route, idx) => (
        <LinkContainer key={idx} to={joinPath(idx)} onlyActiveOnIndex>
          <Breadcrumb.Item >
            {capitalize(route)}
            <FontAwesome name='circle' style={{margin: '0px 10px'}} />
          </Breadcrumb.Item>
        </LinkContainer>
      ))
    }
    </Breadcrumb>
  )
}

AppBreadcrumbs.propTypes = propTypes

export default AppBreadcrumbs
