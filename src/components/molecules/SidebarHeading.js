import React, { PropTypes } from 'react'
import classNames from 'classnames'

const propTypes = {
  title: PropTypes.string.isRequired,
  uppercase: PropTypes.bool
}

const SidebarHeading = (props) => {
  const {
    title,
    uppercase,
    start
  } = props

  return (
    <li className={classNames('heading', { start })}>
      <h3 className={classNames({ uppercase })}>{title}</h3>
    </li>
  )
}

SidebarHeading.propTypes = propTypes

export default SidebarHeading
