import React from 'react'
import { connect } from 'react-redux'

import { I18nextProvider } from 'react-i18next'
import i18n from '../../utils/i18next'

const App = (props, context) => {
  const { children } = props
  // i18n.changeLanguage(lang)

  return (
    <I18nextProvider i18n={i18n}>
      {children}
    </I18nextProvider>
  )
}

export default connect(({ app: lang }) => lang)(App)
