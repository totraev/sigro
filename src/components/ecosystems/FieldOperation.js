import React from 'react'
import { Row, Col, Table } from 'react-bootstrap'
import FontAwesome from 'react-fontawesome'

import Portlet from '../molecules/Portlet'

const FieldOperation = (props) => {
  const iconList = <FontAwesome name='list' />
  const iconEdit = <FontAwesome name='pencil' />

  return (
    <Row>
      <Col md={5}>
        <Portlet icon={iconList} title='Operation Features'>
          <Table className='feature-table' striped responsive>
            <tbody>
              <tr>
                <td className='feature-name'>Name</td>
                <td>Value</td>
                <td className='feature-actions'>{iconEdit}</td>
              </tr>
              <tr>
                <td className='feature-name'>Name</td>
                <td>Value</td>
                <td className='feature-actions'>{iconEdit}</td>
              </tr>
              <tr>
                <td className='feature-name'>Name</td>
                <td>Value</td>
                <td className='feature-actions'>{iconEdit}</td>
              </tr>
            </tbody>
          </Table>
        </Portlet>
      </Col>
    </Row>
  )
}

export default FieldOperation
