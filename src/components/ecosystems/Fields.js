import React from 'react'
import { Row, Col, Table, Pagination } from 'react-bootstrap'
import { Map, TileLayer } from 'react-leaflet'
import { connect } from 'react-redux'

import * as fieldsActions from '../../ducks/fields'

import Icon from '../atoms/Icon'
import RowLink from '../atoms/RowLink'
import Portlet from '../molecules/Portlet'

const Fields = (props) => {
  const fieldIcon = <Icon name='home' />
  const pagination = (
    <Pagination
      items={3}
      bsClass='pagination pagination-circle'
      next
      prev
    />
  )

  return (
    <Row id='field-page'>
      <Col md={5}>
        <Portlet
          title='Fields'
          icon={fieldIcon}
          actions={pagination}
        >
          <Table responsive hover striped>
            <thead onClick={props.fetchFields}>
              <tr>
                <th>Field</th>
                <th>Area</th>
                <th>Culture</th>
              </tr>
            </thead>
            <tbody>
              {
                [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((idx) => (
                  <RowLink to={`/fields/${idx}`} key={idx}>
                    <td>
                      <strong>Field {idx}</strong>
                    </td>
                    <td>1</td>
                    <td>1</td>
                  </RowLink>
                ))
              }
            </tbody>
          </Table>
        </Portlet>
      </Col>

      <Col md={7}>
        <Portlet title='Map'>
          <Map id='map-fields' center={[51.505, -0.09]} zoom={13}>
            <TileLayer url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'/>
          </Map>
        </Portlet>
      </Col>
    </Row>
  )
}

export default connect(({ fields }) => fields, fieldsActions)(Fields)
