import React from 'react'
import { Row, Col, Table } from 'react-bootstrap'
import FontAwesome from 'react-fontawesome'
import { Map, TileLayer } from 'react-leaflet'

import Portlet from '../molecules/Portlet'

const FieldFeatures = (props) => {
  const iconMap = <FontAwesome name='map-marker' />
  const iconList = <FontAwesome name='list' />
  const iconEdit = <FontAwesome name='pencil' />

  return (
    <Row>
      <Col md={5}>
        <Portlet icon={iconList} title='Features'>
          <Table className='feature-table' striped responsive>
            <tbody>
              <tr>
                <td className='feature-name'>Name</td>
                <td>Value</td>
                <td className='feature-actions'>{iconEdit}</td>
              </tr>
              <tr>
                <td className='feature-name'>Name</td>
                <td>Value</td>
                <td className='feature-actions'>{iconEdit}</td>
              </tr>
              <tr>
                <td className='feature-name'>Name</td>
                <td>Value</td>
                <td className='feature-actions'>{iconEdit}</td>
              </tr>
            </tbody>
          </Table>

          <h4><strong>Area:</strong></h4>

          <Table className='feature-table' striped responsive>
            <tbody>
              <tr>
                <td className='feature-name'>Name</td>
                <td>Value</td>
                <td className='feature-actions'>{iconEdit}</td>
              </tr>
              <tr>
                <td className='feature-name'>Name</td>
                <td>Value</td>
                <td className='feature-actions'>{iconEdit}</td>
              </tr>
              <tr>
                <td className='feature-name'>Name</td>
                <td>Value</td>
                <td className='feature-actions'>{iconEdit}</td>
              </tr>
            </tbody>
          </Table>
        </Portlet>
      </Col>

      <Col md={7}>
        <Portlet icon={iconMap} title='Map'>
          <Map style={{height: '425px'}} center={[51.505, -0.09]} zoom={13}>
            <TileLayer url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'/>
          </Map>
        </Portlet>
      </Col>
    </Row>
  )
}

export default FieldFeatures
