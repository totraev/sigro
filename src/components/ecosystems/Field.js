import React from 'react'
import FontAwesome from 'react-fontawesome'
import { LinkContainer } from 'react-router-bootstrap'

import Sidebar from '../organisms/Sidebar'
import Content from '../organisms/Content'

import SidebarItem from '../molecules/SidebarItem'

const Field = (props) => {
  const { children, params: { id } } = props

  const iconOper = <FontAwesome name='external-link' />
  const iconList = <FontAwesome name='list' />
  const iconChart = <FontAwesome name='bar-chart' />
  const iconArea = <FontAwesome name='square-o' />

  return (
    <div id='field-page'>
      <Sidebar>
        <LinkContainer to={`/fields/${id}/features`}>
          <SidebarItem icon={iconList} title='Features' />
        </LinkContainer>
        <LinkContainer to={`/fields/${id}/operations`}>
          <SidebarItem icon={iconOper} title='Operations' />
        </LinkContainer>
        <LinkContainer to={`/fields/${id}/analytics`}>
          <SidebarItem icon={iconChart} title='Analytics' />
        </LinkContainer>
        <LinkContainer to={`/fields/${id}/area`}>
          <SidebarItem icon={iconArea} title='Area' />
        </LinkContainer>
      </Sidebar>

      <Content>
        {children}
      </Content>
    </div>
  )
}

export default Field
