import React from 'react'
import { connect } from 'react-redux'
import * as authActions from '../../ducks/auth'

import Container from '../organisms/Container'
import Footer from '../organisms/Footer'
import Header from '../organisms/Header'

import Clearfix from '../atoms/Clearfix'
import AppBreadcrumbs from '../molecules/AppBreadcrumbs'

const App = (props) => {
  const { location, logout } = props

  return (
    <div className='page-container-bg-solid page-header-fixed  page-footer-fixed page-sidebar-closed-hide-logo'>
      <Header onLogout={() => logout()} />
      <Clearfix />

      <Container>
        <AppBreadcrumbs {...{location}} />
        {props.children}
      </Container>

      <Footer />
    </div>
  )
}

export default connect(({ auth }) => auth, authActions)(App)
