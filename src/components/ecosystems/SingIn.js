import React from 'react'

import SingInForm from '../organisms/SingInForm'

const SingIn = (props) => {
  return (
    <div className='content'>
      <SingInForm />
    </div>
  )
}

export default SingIn
