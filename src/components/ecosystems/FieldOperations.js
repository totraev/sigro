import React from 'react'
import { Row, Col, Table } from 'react-bootstrap'
import FontAwesome from 'react-fontawesome'
import { Map, TileLayer } from 'react-leaflet'

import RowLink from '../atoms/RowLink'
import Portlet from '../molecules/Portlet'

const FieldOperations = (props) => {
  const {params: { id }} = props

  const iconMap = <FontAwesome name='map-marker' />
  const iconList = <FontAwesome name='list' />

  return (
    <Row>
      <Col md={5}>
        <Portlet icon={iconList} title='Operations'>
          <Table striped responsive hover>
            <thead>
              <tr>
                <th>Date</th>
                <th>Opration</th>
                <th>Area</th>
                <th>%</th>
              </tr>
            </thead>
            <tbody>
              <RowLink to={`/fields/${id}/operations/1`}>
                <td>16.02.2016</td>
                <td>Value</td>
                <td>20</td>
                <td>75</td>
              </RowLink>
              <RowLink to={`/fields/${id}/operations/2`}>
                <td>16.02.2016</td>
                <td>Value</td>
                <td>20</td>
                <td>75</td>
              </RowLink>
              <RowLink to={`/fields/${id}/operations/3`}>
                <td>16.02.2016</td>
                <td>Value</td>
                <td>20</td>
                <td>75</td>
              </RowLink>
              <RowLink to={`/fields/${id}/operations/4`}>
                <td>16.02.2016</td>
                <td>Value</td>
                <td>20</td>
                <td>75</td>
              </RowLink>
            </tbody>
          </Table>
        </Portlet>
      </Col>

      <Col md={7}>
        <Portlet icon={iconMap} title='Map'>
          <Map style={{height: '425px'}} center={[51.505, -0.09]} zoom={13}>
            <TileLayer url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'/>
          </Map>
        </Portlet>
      </Col>
    </Row>
  )
}

export default FieldOperations
