import React from 'react'

const AuthLayout = (props) => {
  return (
    <div className='login'>
      <div className='logo'>
        <a href='index.html'>
          <img src={require('../../static/logo.png')} alt='' />
        </a>
      </div>

      {props.children}
      <div className='copyright'> 2014 © Sigro</div>
      <div className='background-img' />
    </div>
  )
}

export default AuthLayout
