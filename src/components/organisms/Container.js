import React from 'react'

const Container = (props) => {
  return (
    <div className='page-container'>
      {props.children}
    </div>
  )
}

export default Container
