import React from 'react'
import FornAwesome from 'react-fontawesome'
import { Button, Alert } from 'react-bootstrap'
import { Field, reduxForm } from 'redux-form'
import { translate } from 'react-i18next'

import AuthCheckbox from '../atoms/AuthCheckbox'
import IconInput from '../molecules/IconInput'

import { login } from '../../ducks/auth'

const SingInForm = (props) => {
  const { handleSubmit, t } = props
  // const renderField = (fieldProps) => <IconInput {...fieldProps} />

  return (
    <form className='login-form' onSubmit={handleSubmit(login)}>
      <h3 className='form-title'>{t('title')}</h3>
      <Alert bsStyle='danger' className='display-hide'>{t('alert')}</Alert>

      <Field
        name='username'
        component={IconInput}
        icon={<FornAwesome name='user' />}
        label={t('username')}
        placeholder={t('username')}
        type='text'
      />
      <Field
        name='password'
        component={IconInput}
        icon={<FornAwesome name='lock' />}
        label={t('password')}
        placeholder={t('password')}
        type='password'
      />

      <div className='form-actions'>
        <AuthCheckbox label={t('remember_me')}/>
        <Button type='submit' className='green pull-right'>{t('submit')}</Button>
      </div>
    </form>
  )
}

const TranslatedSingInForm = translate(['auth'])(SingInForm)

export default reduxForm({ form: 'singin' })(TranslatedSingInForm)
