import React, { PropTypes } from 'react'
import { Nav, NavItem, Grid } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

import Icon from '../atoms/Icon'
import HeaderLogo from '../atoms/HeaderLogo'

const propTypes = {
  onLogout: PropTypes.func
}

const Header = (props) => {
  const { onLogout } = props

  return (
    <div className='page-header navbar navbar-fixed-top'>
      <Grid className='page-header-inner' fluid>
        <HeaderLogo />

        <Nav pullLeft navbar>
          <LinkContainer to='/fields'>
            <NavItem>Fields</NavItem>
          </LinkContainer>
          <NavItem>Technics</NavItem>
          <NavItem>Workers</NavItem>
          <NavItem>Monitoring</NavItem>
        </Nav>

        <Nav pullRight navbar>
          <NavItem onClick={onLogout}>
            <Icon name='logout' />
          </NavItem>
        </Nav>
      </Grid>
    </div>
  )
}

Header.propTypes = propTypes

export default Header
