import React from 'react'

const Footer = (props) => {
  return (
    <div className='page-footer'>
      <div className='page-footer-inner'>
        2016 © Sigro
      </div>
    </div>
  )
}

export default Footer
