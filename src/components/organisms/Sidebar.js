import React from 'react'

const Sidebar = (props) => {
  const { children } = props

  return (
    <div className='page-sidebar navbar-collapse collapse'>
      <ul className='page-sidebar-menu'>
        {children}
      </ul>
    </div>

  )
}

export default Sidebar
