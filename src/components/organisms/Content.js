import React, { PropTypes } from 'react'

const propTypes = {
  title: PropTypes.string,
  breadcrumb: PropTypes.element
}

const Content = (props) => {
  const {
    children,
    title,
    breadcrumb
  } = props

  const head = (
    <div className='page-head'>
      <div className='page-title'>
        <h1>{title}</h1>
      </div>
    </div>
  )

  return (
    <div className='page-content-wrapper'>
      <div className='page-content'>
        {title ? head : null}
        {breadcrumb}
        {children}
      </div>
    </div>
  )
}

Content.propTypes = propTypes

export default Content
