import i18n from 'i18next'

import en from '../locales/en.yml'
import ru from '../locales/ru.yml'

i18n.init({
  fallbackLng: 'ru',
  resources: { ru, en },
  interpolation: {
    escapeValue: false
  }
})

export default i18n
