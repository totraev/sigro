import jwtDecode from 'jwt-decode'

export const setToken = (token) => {
  const decoded = jwtDecode(token)

  localStorage.setItem('token', token)
  localStorage.setItem('tokenExpire', decoded.exp)

  return decoded
}

export const getToken = () => {
  return localStorage.getItem('token')
}

export const isAuth = () => {
  const expireDate = parseInt(localStorage.getItem('tokenExpire'), 10)
  const now = Date.now()

  return expireDate ? now < expireDate * 1000 : false
}

export const removeToken = () => {
  localStorage.clear()
}
