import { createAction } from 'redux-actions'
import { call, put } from 'redux-saga/effects'
import { routeMatcher } from 'route-matcher'

export const createSagaAction = (type) => {
  const REQUEST = `${type}_REQUEST`
  const SUCCESS = `${type}_SUCCESS`
  const FAILURE = `${type}_FAILURE`

  const sagaAction = createAction(REQUEST)

  sagaAction.request = createAction(REQUEST)
  sagaAction.success = createAction(SUCCESS)
  sagaAction.failure = createAction(FAILURE)

  sagaAction.SUCCESS = SUCCESS
  sagaAction.FAILURE = FAILURE
  sagaAction.REQUEST = REQUEST

  sagaAction.getType = () => type
  sagaAction.type = type

  return sagaAction
}

export const fetchSagaCreator = (pattern, method, route, ...params) => {
  const getType = (eventType) => `${pattern}_${eventType}`

  const actionSwitcher = (eventType, payload) => eventType === 'SUCCESS'
    ? pattern.success(payload)
    : pattern.failure(payload)

  const actionCreator = (eventType, payload) => typeof pattern === 'string'
    ? createAction(getType(eventType))(payload)
    : actionSwitcher(eventType, payload)

  const fetchRoute = routeMatcher(route)

  return function * fetch ({ payload }) {
    const fetchPath = fetchRoute.stringify(payload)

    try {
      const data = yield call(method, fetchPath, ...params)
      yield put(actionCreator('SUCCESS', data))
    } catch (e) {
      yield put(actionCreator('FAILURE', e))
    }
  }
}
