import { pathCreator, checkHttpStatus, parseJSON, defaultOptions } from './apiHelpers'
import { getToken } from './auth'
import { tokenPath, apiHost } from '../config'

const apiFetch = (path, options = {}) => {
  return fetch(`${apiHost}${path}`, {
    ...defaultOptions,
    ...options,
    headers: {
      ...defaultOptions.headers,
      ...options.headers
    }
  })
    .then(checkHttpStatus)
    .then(parseJSON)
}

export const login = (userData) => {
  return apiFetch(tokenPath, {
    method: 'POST',
    body: JSON.stringify(userData)
  })
    .then((response) => response.token)
}

export const get = (path) => {
  return apiFetch(pathCreator(path), {
    method: 'GET',
    headers: {
      Authorization: `JWT ${getToken()}`
    }
  })
    .then((response) => response)
}

export const post = (path, body) => {
  return apiFetch(pathCreator(path), {
    method: 'POST',
    headers: {
      Authorization: `JWT ${getToken()}`
    },
    body: JSON.stringify(body)
  })
    .then((response) => response)
}
