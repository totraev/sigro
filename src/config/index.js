import development from './development.json'
import production from './production.json'

module.exports = process.env.NODE_ENV === 'production'
  ? production
  : development
