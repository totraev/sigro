var CopyWebpackPlugin = require('copy-webpack-plugin')
var webpack = require('webpack')
var publicPath = process.env.NODE_ENV === 'production' ? '/static/' : 'http://localhost:3000/'

module.exports = {
  pages: ['index'],
  disabledLoaders: ['yaml'],
  style: {
    cssModules: false
  },
  webpack: {
    output: { publicPath },
    module: {
      loaders: [
        { test: /\.svg(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file' },
        { test: /\.(yaml|yml)$/, loader: 'json!yaml' }
      ]
    },
    plugins: [
      new CopyWebpackPlugin([
        { from: 'locales', to: 'locales' }
      ]),
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
      })
    ]
  }
}
